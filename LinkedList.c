#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *link;
};
struct node *start=NULL;
struct node *createNode()
{
	struct node *n;
	n=(struct node *)malloc(sizeof(struct node));
	return(n);
}
void display()
{
	struct node *p;
	if(start==NULL)
	{
		printf("List is empty");
	}
	p=start;
	while(p!=NULL)
		{
			printf("%d ",p->data);
			p=p->link;
		}
	printf("\n");
		
}

void insertNode(int data)
{
	printf("Insert %d : ",data);
	struct node *temp,*t;
	temp=createNode();
	temp->data=data;
	temp->link=NULL;
	if(start==NULL)
	{
		start=temp;
	}
	else
	{
		t=start;
		while(t->link!=NULL)
			t=t->link;
			
		t->link=temp;	
	}
display();
}
void insertAtBigin(int data)
{
	printf("Insert %d at Beginning : ",data);
	struct node *temp;
	temp=createNode();
	temp->data=data;
	temp->link=start;
	start=temp;
display(); 
}
void deleteNode(int data)
{
	printf("delete %d : ",data);
	struct node *t=start,*prev;
	if(start==NULL)
	{
		printf("Nodes are not available");
	}
	else
	{
		while(t->data!=data)
		{
			prev=t;
			t=t->link;
		}
		prev->link=t->link;
		free(t);
	}
display();
}
void search(int data)
{
	int count=0;
	struct node* t=start;
	while(t->data!=data)
		{
			 count++;
			 t=t->link;
	}
	if(t->data==data)
		printf("Node[%d] contain %d",count+1,data);
		
	else
		printf("%d is not found",data);
	printf("\n");	
}
void reverse()
{
	struct node *prev,*p=start,*temp,*head=start;
	while(p->link!=NULL)
	{
		prev=p;
		p=p->link;
	}
	start=p;
	while(p->link!=head->link)
	{
		temp=head;
		while(temp->link!=p->link)
		{
			prev=temp;
			temp=temp->link;
		}
		p->link=prev;
		p=prev;
	}
	head->link=NULL;
	printf("Reverse List: ");
	display();
}
int main()
{
	insertNode(40);
	insertNode(30);
	insertNode(50);
	insertAtBigin(5);
	insertAtBigin(15);
	insertNode(60);
	insertAtBigin(100);
	search(50);
	insertNode(70);
	deleteNode(30);
	deleteNode(60);
	search(50);
	reverse();
	insertNode(500);
	insertAtBigin(600);
	reverse();
}
